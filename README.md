Галерея фотографий из профиля или группы ВКонтакте
======================================================
Расширение для фреймворка [Yii2](http://www.yiiframework.com/). Отображение на сайте фотографий из профиля или группы ВКонтакте.

Для просмотра фотографий, данное расширение использует jQuery плагин Артема Поликарпова [Fotorama](http://fotorama.io/)

Установка
------------

Предпочтительный способ установки данного расширения — через [composer](http://getcomposer.org/download/).

* Выполните

```
php composer.phar require --prefer-dist eugene-kei/yii2-vk-gallery "*"
```

или добавьте

```
"eugene-kei/yii2-vk-gallery": "*"
```

в секцию `require` в файле `composer.json` вашего приложения.




* Обновите config файл
```
php 
//......
    'modules' => [
        'gallery' => [
            'class' => 'eugenekei\vkgallery\Module',
            'vkOwnerId' => '-xxxxxxx'
        ],
    ],
//......

```
где `-xxxxxxx` — это id (числовой, но не буквенный) профиля или сообщества [Вконтакте](http://vk.com).


Обратите внимание, идентификатор сообщества в параметре `vkOwnerId` необходимо указывать со знаком `-` — например, `-1`.


Подробности смотрите в документации на [http://vk.com/dev/photos.getAlbums](http://vk.com/dev/photos.getAlbums), параметр `owner_id`.



Использование
-----

Галерея доступна по адресу:

```
example.com/index.php?r=gallery
```



