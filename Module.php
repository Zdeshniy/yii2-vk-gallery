<?php

namespace eugenekei\vkgallery;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'eugenekei\vkgallery\controllers';
    
    public $vkApiUrl = 'https://api.vk.com/method/';
    
    public $vkApiVersion = '5.32';
    
    /**
     * id Пользователя или сообщества ВКонтакте, чьи фотоальбомы требуется получить 
     * see http://vk.com/dev/photos.getAlbums for more details
     * @var type string
     */
    public $vkOwnerId = '-1';

    public function init()
    {
        parent::init();
    }
}
