<?php

namespace eugenekei\vkgallery\controllers;

use yii\web\Controller;
use eugenekei\vkgallery\models\VKGallery;
use yii\data\ArrayDataProvider;
use yii\web\HttpException;

class DefaultController extends Controller
{

    public function actionIndex()
    {
        $params = [
            'owner_id'    => $this->module->vkOwnerId,
            'photo_sizes' => 1,
            'need_covers' => 1
        ];

        $model         = new VKGallery;
        $model->params = $params;
        $allAlbums     = $model->getAlbums();
        if (!$allAlbums) {
            throw new HttpException(404);
        }

        $provider = new ArrayDataProvider([
            'allModels'  => $allAlbums,
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);
        return $this->render('index', ['dataProvider' => $provider]);
    }

    public function actionView($id)
    {
        $params        = [
            'owner_id' => $this->module->vkOwnerId,
            'album_id' => $id,
        ];
        $model         = new VKGallery;
        $model->params = $params;
        $albumPhotos   = $model->getPhotos();
        $items         = VKGallery::getItemsArray($albumPhotos);
        $albumTitle    = $model->getAlbumTitle();

        return $this->render('view', ['items' => $items, 'albumTitle' => $albumTitle]);
    }

}
