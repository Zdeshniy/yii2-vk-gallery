<?php

use yii\widgets\ListView;
use eugenekei\vkgallery\Asset;
use yii\web\View;

$this->title = 'Фотоальбомы';
$this->params['breadcrumbs'][] = $this->title;


Asset::register($this);
$js = '$(function () {$("[data-toggle=\'tooltip\']").tooltip()});';
$this->registerJs($js, View::POS_END);
?>

<h1><?= $this->title ?></h1>
    <?php
    echo ListView::widget(
            [
                'dataProvider' => $dataProvider,
                'layout'       => "{items}\n{pager}",
                'itemView'     => '_album_item',
                'options'      => [
                    'class' => 'row'
                ],
                'itemOptions'  => [
                    'class' => 'col-xs-12 col-sm-6 col-md-4 text-center',
                ]
            ]
    );
    ?>
