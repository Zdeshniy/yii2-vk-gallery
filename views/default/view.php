<?php

use eugenekei\vkgallery\Asset;
use yii\helpers\Html;

$this->title = Html::encode($albumTitle);
$this->params['breadcrumbs'][] = ['label' => 'Фотоальбомы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

Asset::register($this);
?>
<h1><?= $this->title ?></h1>
    <!-- Thumbs -->
    <div class="thumbs text-center">
        <div class="fotorama"
             data-auto="false"
             data-allow-full-screen="true"
             data-thumb-width="87"
             data-fit="scaledown"
             data-thumb-height="87"
             data-nav="thumbs">
                 <?php
                 if (is_array($items)&&!empty($items)) {
                     foreach ($items as $item) {
                         $img     = Html::img(Html::encode($item['src']), ['alt' => Html::encode($item['options']['title'])]);
                         $options = ['class' => 'gallery-item', 'title' => Html::encode($item['options']['title'])];

                         echo Html::a($img, Html::encode($item['url']), $options);
                     }
                 }
                 else{
                     echo 'Ничего не найдено';
                 }
                 ?>

        </div>
    </div>