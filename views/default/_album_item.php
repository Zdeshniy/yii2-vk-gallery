<?php

/**
 * VKGallery albums list item view.
 *
 * @var \yii\web\View $this View
 * @var eugenekei\vkgallery\models\VKGallery $model Model
 */
use yii\helpers\Html;

$imgHtmlOptions     = [
    'alt' => Html::encode($model->title),
];
$tooltipHtmlOptions = [];

if (!empty($model->description)) {
    $tooltipHtmlOptions = [
        'data' => [
        'toggle'    => 'tooltip',
        'placement' => 'top',
        'html' => 'true',
        ],
        'title'          => Html::encode($model->description),
    ];
}

if ($model->sizes[2]->width > $model->sizes[2]->height) {
    $imgHtmlOptions['class'] = 'horizontal';
}
?>

<?php echo Html::beginTag('div', $tooltipHtmlOptions); ?>
<div class="center-cropped center-block">
    <?=
    Html::a(
            Html::img(Html::encode($model->sizes[2]->src), $imgHtmlOptions), ['view', 'id' => (int) $model->id], $tooltipHtmlOptions
    )
    ?>
</div>
<h5>
    <?php echo Html::a(Html::encode($model->title), ['view', 'id' => (int) $model->id]); ?>
</h5>

<div class="entry-meta">
    <span><i class="glyphicon glyphicon-time"></i> 
        <?php echo Yii::$app->formatter->asDatetime($model->created); ?>
    </span>
    &nbsp;&nbsp;&nbsp;
    <span><i class="glyphicon glyphicon-film"></i> 
        <?php echo Html::encode($model->size); ?>
    </span>
</div>
<?php echo Html::endTag('div'); ?>
