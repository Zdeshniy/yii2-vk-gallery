<?php

namespace eugenekei\vkgallery\models;

use yii\base\Model;
use eugenekei\vkgallery\Module;
use yii\web\HttpException;

class VKGallery extends Model
{

    public $params;
    protected $response;

    public function getAlbums()
    {
        if (!$this->params) {
            return false;
        }
        $this->response = json_decode($this->queryApi('photos.getAlbums', $this->params));
        return $this->checkResponse();
    }

    public function getAlbumTitle()
    {
        if (empty($this->params['album_id']) || empty($this->params['owner_id'])) {
            return null;
        }
        $params                    = [
            'album_ids' => $this->params['album_id'],
            'owner_id'  => $this->params['owner_id']
        ];

        $this->response = json_decode($this->queryApi('photos.getAlbums', $params));
        return $this->checkResponse()['0']->title;
    }

    public function getPhotos()
    {
        if (!$this->params) {
            return false;
        }
        $this->response = json_decode($this->queryApi('photos.get', $this->params));
        return $this->checkResponse();
    }

    public static function getItemsArray($photosResponse)
    {
        if (!is_array($photosResponse)) {
            return false;
        }
        $itemsArray = [];
        foreach ($photosResponse as $photo) {
            if (isset($photo->sizes)) {
                $itemsArray[] = [
                    'url'     => $photo->sizes[3]->src,
                    'src'     => $photo->sizes[1]->src,
                    'options' => ['title' => $photo->text],
                ];
            } else {
                $srcBig       = isset($photo->photo_807) ? $photo->photo_807 : $photo->photo_604;
                $itemsArray[] = [
                    'url'     => $srcBig,
                    'src'     => $photo->photo_130,
                    'options' => ['title' => $photo->text],
                ];
            }
        }
        return $itemsArray;
    }

    protected function checkResponse()
    {
        if (!isset($this->response->response)) {
            if (isset($this->response->error)) {
                throw new HttpException(400, $this->response->error->error_msg);
            }
            return false;
        }
        return $this->response->response->items;
    }

    protected function queryApi($method, array $params)
    {
        $versionAPI           = Module::getInstance()->vkApiVersion;
        $httpBuildQueryParams = http_build_query($params);
        $url                  = Module::getInstance()
                ->vkApiUrl . $method . '?' . $httpBuildQueryParams . '&v=' . $versionAPI;

        return file_get_contents($url);
    }

}
