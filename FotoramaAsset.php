<?php

namespace eugenekei\vkgallery;

use yii\web\AssetBundle;

/**
 * Module asset bundle.
 */
class FotoramaAsset extends AssetBundle {

    public $sourcePath = '@vendor/bower/fotorama';
    
    public $css = [
        'fotorama.css'
    ];
    
    public $js = [
        'fotorama.js'
    ];
    
    public $depends = [
        'yii\web\JqueryAsset',
    ];

}
