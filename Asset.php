<?php

namespace eugenekei\vkgallery;

use yii\web\AssetBundle;

/**
 * Module asset bundle.
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@eugenekei/vkgallery/assets';

    public $css = [
        'css/VKGallery.css',
    ];
    
    public $js = [
        'js/lightboxfotorama.js'
    ];
    
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'eugenekei\vkgallery\FotoramaAsset'
    ];
}
